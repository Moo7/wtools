#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
# Michel Mooij, michel.mooij7@gmail.com

'''
Summary
-------
This module is deprecated and contains no logic. It exists only for backwards 
compatibility.
Please use wtools.make instead
'''

def options(opt):
    pass

def configure(conf):
    pass
