#include <stdio.h>

#include "bar.h"

void bar(const char *name)
{
    printf("Hi there %s, please to meet you\n", name);
}
