#include <stdio.h>

#include "foo.h"
#include <bar.h>

void foo(const char *name)
{
    bar(name);
}

