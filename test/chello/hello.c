#include <stdio.h>

#include "hello.h"

void greet(const char *name)
{
    printf("Hi there %s, please to meet you\n", name);
}
