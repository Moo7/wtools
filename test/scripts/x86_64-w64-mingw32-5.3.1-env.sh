export SYSROOT=/usr/x86_64-w64-mingw32/

export HOST=x86_64-w64-mingw32
export CC="$HOST-gcc"
export CXX="$HOST-g++"
export CPP="$HOST-gcc"
export RANLIB=$HOST-ranlib
export AR=$HOST-ar
export NM=$HOST-nm
export WINDRES=$HOST-windres
export M4=m4
export TARGET_PREFIX=$HOST-
export CONFIGURE_FLAGS="--target=$HOST --host=$HOST --build=x86_64-linux"
export CFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack"
export CXXFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack"
export LDFLAGS="-L$SYSROOT/lib"
export CPPFLAGS=""
export ARCH=x86_64
export CROSS_COMPILE=$HOST-
export EROOT="./ext/out/opt/`$CC -dumpmachine`-`$CC -dumpversion`"

unset PKG_TYPE

