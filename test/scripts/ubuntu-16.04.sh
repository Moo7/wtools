unset BASE

unset SDKTARGETSYSROOT
unset SYSROOT
unset RPMBUILD
unset CCACHE_PATH

unset PKG_CONFIG_SYSROOT_DIR
unset PKG_CONFIG_PATH
unset CONFIG_SITE
unset OECORE_NATIVE_SYSROOT
unset OECORE_TARGET_SYSROOT
unset OECORE_ACLOCAL_OPTS
unset HOST

unset BINBASE

unset CC
unset CXX
unset CPP
unset AS
unset LD
unset GDB
unset STRIP
unset RANLIB
unset OBJCOPY
unset OBJDUMP
unset AR
unset NM
unset M4

unset TARGET_PREFIX
unset CONFIGURE_FLAGS
unset KCFLAGS
unset OECORE_DISTRO_VERSION
unset OECORE_SDK_VERSION
unset ARCH
unset CROSS_COMPILE
unset HOST
unset RPM_TARGET

unset LIBPATH
unset INCLUDES

export CFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack"
export CXXFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack"
export LDFLAGS="-Wl,--as-needed"
export CPPFLAGS=""
export EROOT="./ext/out"
