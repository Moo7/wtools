BASE=$HOME/.local/opt/arm-poky-linux-gnueabi/sysroots

export SDKTARGETSYSROOT=$BASE/cortexa9hf-neon-poky-linux-gnueabi
export SYSROOT=$SDKTARGETSYSROOT/usr

export RPMBUILD=$HOME/.local/opt/arm-poky-linux-gnueabi/sysroots/x86_64-pokysdk-linux/usr/src/rpm

export PATH=$BASE/x86_64-pokysdk-linux/usr/bin:$BASE/x86_64-pokysdk-linux/usr/sbin:$BASE/x86_64-pokysdk-linux/bin:$BASE/x86_64-pokysdk-linux/sbin:$BASE/x86_64-pokysdk-linux/usr/bin/../x86_64-pokysdk-linux/bin:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-uclibc:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-musl:$PATH

export CCACHE_PATH=$BASE/x86_64-pokysdk-linux/usr/bin:$BASE/x86_64-pokysdk-linux/usr/bin/../x86_64-pokysdk-linux/bin:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-uclibc:$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-musl:$CCACHE_PATH

export PKG_CONFIG_SYSROOT_DIR=$SDKTARGETSYSROOT
export PKG_CONFIG_PATH=$SDKTARGETSYSROOT/usr/lib/pkgconfig
export CONFIG_SITE=$HOME/.local/opt/arm-poky-linux-gnueabi/site-config-cortexa9hf-neon-poky-linux-gnueabi
export OECORE_NATIVE_SYSROOT="$BASE/x86_64-pokysdk-linux"
export OECORE_TARGET_SYSROOT="$SDKTARGETSYSROOT"
export OECORE_ACLOCAL_OPTS="-I $BASE/x86_64-pokysdk-linux/usr/share/aclocal"
unset command_not_found_handle

BINBASE=$BASE/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi

export CC="$BINBASE/arm-poky-linux-gnueabi-gcc -march=armv7-a -marm -mfpu=neon  -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=$SDKTARGETSYSROOT"
export CXX="$BINBASE/arm-poky-linux-gnueabi-g++ -march=armv7-a -marm -mfpu=neon  -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=$SDKTARGETSYSROOT"
export CPP="$BINBASE/arm-poky-linux-gnueabi-gcc -E -march=armv7-a -marm -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=$SDKTARGETSYSROOT"
export AS="$BINBASE/arm-poky-linux-gnueabi-as"
export LD="$BINBASE/arm-poky-linux-gnueabi-ld"
export GDB="$BINBASE/arm-poky-linux-gnueabi-gdb"
export STRIP="$BINBASE/arm-poky-linux-gnueabi-strip"
export RANLIB="$BINBASE/arm-poky-linux-gnueabi-ranlib"
export OBJCOPY="$BINBASE/arm-poky-linux-gnueabi-objcopy"
export OBJDUMP="$BINBASE/arm-poky-linux-gnueabi-objdump"
export AR="$BINBASE/arm-poky-linux-gnueabi-ar"
export NM="$BINBASE/arm-poky-linux-gnueabi-nm"
export M4=m4
export TARGET_PREFIX=arm-poky-linux-gnueabi-
export CONFIGURE_FLAGS="--target=arm-poky-linux-gnueabi --host=arm-poky-linux-gnueabi --build=x86_64-linux --with-libtool-sysroot=$SDKTARGETSYSROOT"
export CFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack -pipe -g -feliminate-unused-debug-types"
export CXXFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -Wtrampolines -z execstack -pipe -g -feliminate-unused-debug-types"
export LDFLAGS="-Wl,--hash-style=gnu -Wl,--as-needed --sysroot=$SDKTARGETSYSROOT"
export CPPFLAGS="-I$SYSROOT/include"
export KCFLAGS="--sysroot=$SDKTARGETSYSROOT"
export OECORE_DISTRO_VERSION="1.0.0"
export OECORE_SDK_VERSION="2.1"
export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-
export HOST=arm-poky-linux-gnueabi
export EROOT="./ext/out/opt/`$CC -dumpmachine`-`$CC -dumpversion`"
export RPM_TARGET=cortexa9hf_neon-pokysdk-linux


# Append environment subscripts
if [ -d "$OECORE_TARGET_SYSROOT/environment-setup.d" ]; then
    for envfile in $OECORE_TARGET_SYSROOT/environment-setup.d/*.sh; do
        source $envfile
    done
fi
if [ -d "$OECORE_NATIVE_SYSROOT/environment-setup.d" ]; then
    for envfile in $OECORE_NATIVE_SYSROOT/environment-setup.d/*.sh; do
        source $envfile
    done
fi

export PKG_TYPE=rpm
