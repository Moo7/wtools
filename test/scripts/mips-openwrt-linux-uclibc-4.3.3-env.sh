BASE=$HOME/.local/opt/openwrt-mips-backfire/toolchain-mips_r2_gcc-4.3.3+cs_uClibc-0.9.30.1

export PATH=$BASE/usr/bin:$PATH
export STAGING_DIR=$BASE/usr
export SYSROOT=$BASE/usr/mips-openwrt-linux

BINBASE="$BASE/usr/bin"

export HOST=mips-openwrt-linux
export CC="$BINBASE/$HOST-gcc"
export CXX="$BINBASE/$HOST-g++"
export CPP="$BINBASE/$HOST-gcc"
export AR="$BINBASE/$HOST-ar"
export STRIP="$BINBASE/$HOST-strip"
export LD="$BINBASE/$HOST-ld"
export RANLIB="$BINBASE/$HOST-ranlib"
export OBJCOPY="$BINBASE/$HOST-objcopy"
export OBJDUMP="$BINBASE/$HOST-objdump"
export AS="$BINBASE/$HOST-as"
export NM="$BINBASE/$HOST-nm"
export M4=m4
export TARGET_PREFIX=$HOST-
export CONFIGURE_FLAGS="--target=$HOST --host=$HOST --build=x86_64-linux"
export CFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -I$BASE/usr/include -I$SYSROOT/include"
export CXXFLAGS="-Wall -Wextra -Wconversion -Warray-bounds -I$BASE/usr/include -I$SYSROOT/include"
export LDFLAGS="-Wl,--as-needed"
export CPPFLAGS=""
export ARCH=ar71xx
export CROSS_COMPILE=$HOST-
export EROOT="./ext/out/opt/`$CC -dumpmachine`-`$CC -dumpversion`"

export PKG_TYPE=ipk
